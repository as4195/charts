import {Component, OnInit} from '@angular/core';
import {ChartsService} from './charts.service';
import * as Highcharts from 'highcharts';
declare var require: any;
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/solid-gauge')(Highcharts);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  highcharts = Highcharts;
  chartOptions: any;
  charts: any;
  chartHolder;
  solidGauge: any;

  constructor(private chartS: ChartsService) {
  }

  ngOnInit() {
    this.chartS.data1().subscribe((data) => {
      this.charts = data;
      this.chartOptions = this.charts.hits.hits;
    });
    this.chartS.data2().subscribe((data) => {
      this.charts = data;
      this.solidGauge = this.charts.hits.hits;
      this.chartHolder = Highcharts.chart('container-speed', this.solidGauge[0]._source);
    });
    setInterval(() => {
      // Speed
      var point,
        newVal,
        inc;

      if (this.solidGauge[0]._source) {
        point = this.solidGauge[0]._source.series[0].data[0];
        inc = Math.round((Math.random() - 0.5) * 100);
        newVal = point + inc;

        if (newVal < 0 || newVal > 200) {
          newVal = point - inc;
        }
        this.solidGauge[0]._source.series[0].data[0] = newVal;
        this.chartHolder = Highcharts.chart('container-speed', this.solidGauge[0]._source);
      }
    }, 2000);
  }
}
